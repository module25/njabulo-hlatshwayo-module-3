import 'package:flutter/material.dart';
import './ui/login.dart';
import './ui/register.dart';
import 'login.dart';
import 'register.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MTN APP',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: {
        'register': (context) => RegisterPage(
              title: '',
            )
      },
      home: LoginPage(title: 'Login'),
      debugShowCheckedModeBanner: false,
    );
  }
}
